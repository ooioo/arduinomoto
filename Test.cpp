// #define DEBUG

#include <Arduino.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "Test.h"

Test::Test() {
}

void Test::go() const {
  testItem(CG_LED_PIN);
  testItem(CD_LED_PIN);
  testItem(STOP_LED_PIN);
  testItem(PHARE_LED_PIN);
  testItem(CODE_LED_PIN);
  testItem(POINTMORT_LED_PIN);
  testItem(CTEMOIN_LED_PIN);
  tone(KLAXON_PIN, 1000, 500);
}

void Test::testItem(int item) const {
  for (int i = 1; i <= 3; i++) {
    digitalWrite(item, HIGH);
    delay(TEMPO_TEST);
    digitalWrite(item, LOW);
    delay(TEMPO_TEST);
  }
}
