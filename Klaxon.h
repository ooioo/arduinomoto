#ifndef KLAXON_H_
#define KLAXON_H_

#include <AceButton.h>
#include "ArduinoPins.h"

class Klaxon {
  private:
    ace_button::ButtonConfig config;
    ace_button::AceButton button;
    void static handleEvent(ace_button::AceButton*, uint8_t, uint8_t);
  public:
    Klaxon();
    void go();
};

#endif
