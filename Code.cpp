#include <Arduino.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "Code.h"

Code::Code() {
  pinMode(CODE_LED_PIN, OUTPUT);
}

void Code::go() const {
  // les codes sont allumés dés que l'arduino est en marche
  digitalWrite(CODE_LED_PIN, HIGH);
}
