#ifndef TEST_H_
#define TEST_H_
#define TEMPO_TEST 100

class Test {
  public:
    Test();
    void go() const;
  private:
    void testItem(int item) const;
};

#endif
