#ifndef VItESSE_H_
#define VItESSE_H_

#define UPDATE 250   // Millisecondes pour le rafraichissement de l'affichage
#define UPDATEMAX 5000 // Si pas d'interruption dans ce délai, afficher vitesse 0
// vitesse en km/h = ( circonférence de la roue en m / 1000 ) / ( periode en ms / 3600 * 1000 )
//  si n capteurs sur la roue, diviser CIRCONFERENCE par n
#define CIRCONFERENCE 7200.0  // 3600 fois la circonférence en m de la roue portant le capteur
#define MILLIVMAX 10  // 10 milliseconde pour un tour de roue, plus de 700 km/h !

#include "ArduinoPins.h"

class Vitesse {
  public:
    Vitesse();
    void go();
  private:
    static volatile unsigned long millisInt1;
    static volatile unsigned long millisInt2;
    unsigned long millisDisplay;
    void static interrupt();
};

#endif
