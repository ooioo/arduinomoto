#ifndef CODE_H_
#define CODE_H_

#include "ArduinoPins.h"

class Code {
  public:
    Code();
    void go() const;
};

#endif
