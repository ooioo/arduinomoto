// #define DEBUG

#include <Arduino.h>
#include <AceButton.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "Stop.h"

using namespace ace_button;

Stop::Stop(): config(), button(STOP_BUTTON_PIN) {
  pinMode(STOP_LED_PIN, OUTPUT);
  pinMode(STOP_BUTTON_PIN, INPUT_PULLUP);
  button.setButtonConfig(&config);
  config.setEventHandler(handleEvent);
}

void Stop::go() {
  button.check();
}

void Stop::handleEvent(AceButton*, uint8_t eventType, uint8_t) {
  switch (eventType) {
    case AceButton::kEventPressed:
      digitalWrite(STOP_LED_PIN, HIGH);
      break;
    case AceButton::kEventReleased:
      digitalWrite(STOP_LED_PIN, LOW);
      break;
  }
}
