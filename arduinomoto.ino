
/*
   Gestion des lumières et de l'avertisseur d'une moto
   Nécessite l'installation des libs AceButton et Chrono

   Cligno est un clignotant contrôlé par deux boutons poussoir
   Cligno2 est un clignotant contrôlé par une manette à 3 positions
     gauche, arrêt, droit
   Vous pouvez supprimer les fichiers .cpp et .h inutilisés

   Vitesse est une mesure de la vitesse avec un interrupteur à effet hall
     positionné sur la roue. La gestion de l'affichage n'est pas faite.
   Si pas de compteur de vitesse vous pouvez supprimer les fichiers
   vitesse.cpp et vitesse.h ainsi que #include "Vitesse.h" dans ce fichier

   Les définitions de broches d'entrée sortie de l'arduino sont
   dans le fichier ArduinoPins.h
*/
// #define DEBUG

#include <Arduino.h>
#include "Debug.h"
#include "Code.h"
#include "Cligno2.h"
#include "Stop.h"
#include "Phare.h"
#include "Klaxon.h"
#include "Vitesse.h"
#include "PointMort.h"
#include "Test.h"

Code code;
// Déclarer ici Cligno ou Cligno2 selon votre commande de clignotant
Cligno2 cligno;
Stop feuStop;
Klaxon klaxon;
Phare phare;
Vitesse vitesse;
PointMort pointMort;
Test test;

void setup() {
  SERIAL_BEGIN(115200);
  // Fait fonctionner successsivement tous les accessoires
  //  pour tester leur bon fonctionnement
  test.go();
  DEBUG_PRINT("tests terminés");
  // Allume les codes
  code.go();
  DEBUG_PRINT("code allumé");
}

// Boucle infinie appelée par l'arduino pour gérer
//  l'état de tous les accessoires
void loop() {
  cligno.go();
  feuStop.go();
  phare.go();
  klaxon.go();
  vitesse.go();
  pointMort.go();
}
