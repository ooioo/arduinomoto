/*
 * Clignotant pousse bouton
 * Un appui sur le bouton clignotant droit ou gauche
 *  met en fonctionnement le clignotant s'il ne l'était pas
 *    sinon il arrête son fonctionnement
 * Un double click sur un bouton déclenche ou arrête les
 *   feux de détresse (clignotement simultané de deux clignotants)
 */
#ifndef CLIGNO_H_
#define CLIGNO_H_

#include <Chrono.h>
#include <AceButton.h>
#include "ArduinoPins.h"

#define C_TEMPO_BLINK 500 // durée du clignotement en ms
#define C_TEMPO_MAX 20000 // durée max du clignotement en ms

class Cligno
{
  private:
    ace_button::ButtonConfig config; // la config liée aux deux boutons
    ace_button::AceButton buttonG; // le bouton clignotant gauche
    ace_button::AceButton buttonD; // le bouton clignotant droit
    void static handleEvent(ace_button::AceButton*, uint8_t, uint8_t);
    bool static cgOn;          // Clignotant gauche en fonctionnement
    bool static ledCgOn;       // true si la led CG est allumée
    bool static cdOn;          // Clignotant droit en fonctionnement
    bool static ledCdOn;       // true si la led CD est allumée
    bool static warningOn;     // Warning en fonctionnement
    Chrono static chronoBlink; // Chrono pour durée du clignotement
    Chrono static chronoMax;   // Chrono pour arrêt clignotant
  public:
    Cligno();
    void go();
};

#endif
