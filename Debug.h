// Si les #define DEBUG sont en commentaires dans les fichiers
//  le préprocesseur remplace par une chaîne vide :
//  SERIAL_BEGIN(str) et DEBUG_PRINT(str)
// sinon des messages sont affichés dans la console de l'IDE
//   la vitesse est définie dans arduinomoto.ino
//   Il faut donner la même vitesse dans la vue du moniteur série de l'IDE
#ifndef DEBUGUTILS_H
#define DEBUGUTILS_H

#include <Arduino.h>

#ifdef DEBUG
#define SERIAL_BEGIN(speed) delay(100);Serial.begin(speed);
#else
#define SERIAL_BEGIN(str)
#endif

#ifdef DEBUG
#define DEBUG_PRINT(str)    \
   Serial.print(millis());     \
   Serial.print(": ");    \
   Serial.print(__PRETTY_FUNCTION__); \
   Serial.print(' ');      \
   Serial.print(__FILE__);     \
   Serial.print(':');      \
   Serial.print(__LINE__);     \
   Serial.print(' ');      \
   Serial.println(str);
#else
#define DEBUG_PRINT(str)
#endif

#endif
