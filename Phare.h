#ifndef PHARE_H_
#define PHARE_H_

#include <AceButton.h>
#include "ArduinoPins.h"

class Phare {
  private:
    ace_button::ButtonConfig config;
    static bool phareOn;
    ace_button::AceButton button;
    // https://stackoverflow.com/questions/12662891/how-can-i-pass-a-member-function-where-a-free-function-is-expected
    void static handleEvent(ace_button::AceButton*, uint8_t, uint8_t);
  public:
    Phare();
    void go();
};

#endif
