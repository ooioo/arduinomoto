#ifndef STOP_H_
#define STOP_H_

#include <AceButton.h>
#include "ArduinoPins.h"

class Stop {
    ace_button::ButtonConfig config;
    ace_button::AceButton button;
    void static handleEvent(ace_button::AceButton*, uint8_t, uint8_t);
  public:
    Stop();
    void go();
};

#endif
