// #define DEBUG

#include <Arduino.h>
#include "Debug.h"
#include <AceButton.h>
#include <Chrono.h>
#include "ArduinoPins.h"
#include "Cligno2.h"

using namespace ace_button;

bool Cligno2::cgOn = false;
bool Cligno2::ledCgOn = false;
bool Cligno2::cdOn = false;
bool Cligno2::ledCdOn = false;
Chrono Cligno2::chronoBlink;

Cligno2::Cligno2() : config(),
		buttonG(CG_BUTTON_PIN, HIGH, 0), buttonD(CD_BUTTON_PIN, HIGH, 1) {
	// La led du clignotant gauche
	pinMode(CG_LED_PIN, OUTPUT);
	pinMode(CG_BUTTON_PIN, INPUT_PULLUP);
	buttonG.setButtonConfig(&config);
	// La led du clignotant droit
	pinMode(CD_LED_PIN, OUTPUT);
	pinMode(CD_BUTTON_PIN, INPUT_PULLUP);
	buttonD.setButtonConfig(&config);
	config.setEventHandler(handleEvent);
	// La led du témoin de clignotant
	pinMode(CTEMOIN_LED_PIN, OUTPUT);
	// voir AceButton/src/ace_button/ButtonConfig.h et AceButton.h pour les événements
}

void Cligno2::go() {
	// Est-ce que l'état d'un bouton a changé ?
	buttonG.check();
	buttonD.check();
	// Gestion du clignotement si un des clignotants est en fontionnement
	if (cgOn && chronoBlink.hasPassed(C_TEMPO_BLINK)) {
		// Si le clignotant gauches est en fonctionnement
		// et que la tempo C_TEMPO_BLINK est dépassée, on inverse
		// l'état du clignotant (lumière éteinte/allumée)
		digitalWrite(CG_LED_PIN, ledCgOn ? LOW : HIGH);
		digitalWrite(CTEMOIN_LED_PIN, ledCgOn ? LOW : HIGH);
		ledCgOn = !ledCgOn;
		chronoBlink.restart();
	} else if (cdOn && chronoBlink.hasPassed(C_TEMPO_BLINK)) {
		// idem pour le clignotant droit
		digitalWrite(CD_LED_PIN, ledCdOn ? LOW : HIGH);
		digitalWrite(CTEMOIN_LED_PIN, ledCdOn ? LOW : HIGH);
		ledCdOn = !ledCdOn;
		chronoBlink.restart();
	}
}

void Cligno2::handleEvent(AceButton *button, uint8_t eventType, uint8_t) {
	DEBUG_PRINT(button->getId());
	DEBUG_PRINT(eventType);
	switch (eventType) {
	case AceButton::kEventReleased:
		DEBUG_PRINT("interrupteur: arrêt d'un des clignotants");
		// Si interrupteur: arrêt d'un des clignotants on force l'extinction
		//  des clignos et du témoin
		cdOn = false;
		digitalWrite(CD_LED_PIN, LOW);
		cgOn = false;
		digitalWrite(CG_LED_PIN, LOW);
		digitalWrite(CTEMOIN_LED_PIN, LOW);
		break;
	case AceButton::kEventPressed: // AceButton::kEventLongPressed:
		if (button->getId() == 0) {
			// interrupteur cligno gauche activé
			DEBUG_PRINT("cligno gauche activé");
			if (!cgOn) {
				// Le clignotant gauche est éteint
				// On force l'extinction du clignotant droit par sécurité
				cdOn = false;
				digitalWrite(CD_LED_PIN, LOW);
				// On allume le clignotant gauche
				digitalWrite(CG_LED_PIN, HIGH);
				// On allume le témoin
				digitalWrite(CTEMOIN_LED_PIN, HIGH);
				ledCgOn = true;
				//  On démarre la tempo pour durée du clignotement
				chronoBlink.restart();
				// On mémorise qu'il est en fonctionnement
				cgOn = true;
			}
		} else { // button.getId() == 1
			// bouton cligno droit enfoncé
			DEBUG_PRINT("cligno droit activé");
			if (!cdOn) {
				// Le clignotant droit est éteint
				// On force l'extinction du clignotant gauche par sécurité
				cgOn = false;
				digitalWrite(CG_LED_PIN, LOW);
				// On allume le clignotant droit
				digitalWrite(CD_LED_PIN, HIGH);
				// On allume le témoin
				digitalWrite(CTEMOIN_LED_PIN, HIGH);
				ledCdOn = true;
				chronoBlink.restart();
				cdOn = true;
			}
		}
		break;
	}
}
