#ifndef PINS_H_
#define PINS_H_

// CG clignotants gauche
#define CG_LED_PIN 2
#define CG_BUTTON_PIN 3
// CD clignotants droit
#define CD_LED_PIN 4
#define CD_BUTTON_PIN 5
// Stop
#define STOP_LED_PIN 6
#define STOP_BUTTON_PIN 7
// Phare
#define PHARE_LED_PIN 8
#define PHARE_BUTTON_PIN 9
// Klaxon
#define KLAXON_PIN 10
#define KLAXON_BUTTON_PIN 14
// Code
#define CODE_LED_PIN 15
// Vitesse
#define VITESSE_INPUT_PIN 16
// Point mort
#define POINTMORT_LED_PIN 17
#define POINTMORT_BUTTON_PIN 18
// Témoin de clignotant
#define CTEMOIN_LED_PIN 19


#endif
