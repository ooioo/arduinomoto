## Gestion du faisceau électrique d'une moto avec un [Arduino](https://www.arduino.cc/).
- Clignotants, témoin de clignotants : boutons poussoirs clignotant droit et gauche, un appui court démarre ou arrête le clignotant. Les clignotants s'arrêtent automatiquement après un délai (voir Cligno.h). Double clic : allumage des feux de détresse, tous les clignotants fonctionnent.
- Stop : bouton poussoir, allume le feu stop quand le bouton est appuyé, l'éteint que le bouton est relaché.
- Phare, témoin de phare : bouton poussoir, appui long appel de phare, appui court allume/éteint le phare
- Klaxon : bouton poussoir, démarre le klaxon quand le bouton est appuyé, l'éteint que le bouton est relaché.
- Témoin de point mort : bouton poussoir, allume le témoin quand le bouton est fermé, l'éteint que le bouton est ouvert.
- Code, feu arrière : allumés dès que l'Arduino est sous tension
- Vitesse : mesure de la vitesse avec un capteur sur la roue (donner la circonférence de la roue dans Vitesse.h). L'affichage de la vitesse n'est géré qu'en mode debug dans la vue Serial Monitor.
- Test : fait clignoter successivement toutes les lumières à l'allumage de l'Arduino et actionne le buzzer
## Cablage de l'Arduino
- Les boutons sont en pullup interne (INPUT_PULLUP). Ils doivent donc relier leur broche (pin arduino) d'entrée à la masse.
- Le capteur de vitesse pour la roue est aussi en pullup interne, relier sa broche à la masse par l'intermédiaire du capteur
   (interrupteur à effet Hall déclenché par le passage d'un aimant fixé sur la roue)
- Pour les numéros des broches à utiliser, voir le fichier ArduinoPins.h
- Sur une platine d'expérimentation (breadboard), relier les leds et le buzzer de test par une résistance de 220 Ohms à leurs broches de sortie de l'Arduino
## Divers
- Il faut installer les bibliothèques [AceButton](https://github.com/bxparks/AceButton) et [Chrono](https://github.com/SofaPirate/Chrono) ("Outils/Gérer les bibliothèques" dans l'ide arduino)
- Pour debug, ajouter ou décommenter "#define DEBUG" en tête du fichier arduino_moto.ino et du fichier à debogguer et insérer des "DEBUG_PRINT("xxx");"
  - afficher les sorties dans la console ("Outils/Moniteur série" dans l'ide arduino)
  - les "#DEBUG_PRINT("xxx");" peuvent rester dans le code, ils sont ignorés si "#define DEBUG" est en commentaire.
## Schéma
![](./schema.png)



