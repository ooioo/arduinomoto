// #define DEBUG

#include <Arduino.h>
#include <AceButton.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "PointMort.h"

using namespace ace_button;

PointMort::PointMort(): config(), button(POINTMORT_BUTTON_PIN) {
  pinMode(POINTMORT_LED_PIN, OUTPUT);
  pinMode(POINTMORT_BUTTON_PIN, INPUT_PULLUP);
  button.setButtonConfig(&config);
  config.setEventHandler(handleEvent);
}

void PointMort::go() {
  button.check();
}

void PointMort::handleEvent(AceButton*, uint8_t eventType, uint8_t) {
  switch (eventType) {
    case AceButton::kEventPressed:
      digitalWrite(POINTMORT_LED_PIN, HIGH);
      break;
    case AceButton::kEventReleased:
      digitalWrite(POINTMORT_LED_PIN, LOW);
      break;
  }
}
