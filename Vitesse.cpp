// #define DEBUG

#include <Arduino.h>
#include <util/atomic.h>
#include <math.h>
#include "ArduinoPins.h"
#include "Vitesse.h"
#include "Debug.h"

// https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
// http://gammon.com.au/interrupts
// https://www.arduino.cc/en/pmwiki.php?n=Reference/Volatile
// https://www.arduino.cc/reference/tr/language/variables/variable-scope--qualifiers/volatile/
volatile unsigned long Vitesse::millisInt1 = 0;
volatile unsigned long Vitesse::millisInt2 = MILLIVMAX;

Vitesse::Vitesse() : millisDisplay(0) {
	pinMode(VITESSE_INPUT_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VITESSE_INPUT_PIN), interrupt,
			FALLING);
}

void Vitesse::go() {
	unsigned int vitesse = 0;
	unsigned long millisNow = millis();
	unsigned long m2;
	unsigned long m1;
	// Pour éventuel calcul d'accélération
	// unsigned long m0;
	unsigned long millisPeriode;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { m2 = millisInt2; m1 = millisInt1;}
	// ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { m2 = millisInt2; m1 = millisInt1; m0 = millisInt0;}
	millisPeriode = m2 - m1;
	if ((millisNow - millisDisplay) >= UPDATE) {
		if (millisPeriode < MILLIVMAX) {
			// si millisPeriode < MILLIVMAX, 10 milliseconde pour un tour de roue, plus de 700 km/h !
			// on ignore cette valeur, sans doute un raté du capteur
			DEBUG_PRINT("millisPeriode < 10");
			return;
		}
		if ((millisNow - m2) > UPDATEMAX) {
			// si millisNow - millisPrecedent > UPDATEMAX : il n'y a pas eu de nouvelle interruption depuis UPDATEMAX
			//   vitesse inférieure à 1km/h ou arrêté
			vitesse = 0;
		} else {
			// Accélération : gamma = (v - v0) / t
			// gammma = CIRCONFERENCE * ( (1 / millisPeriode) - (1 / (m1 -m0))) / millisPeriode
			// si gamma suprérieur à 5 g, return
			vitesse = round(CIRCONFERENCE / millisPeriode);
		}
		millisDisplay = millisNow;
		// L'affichage de la vitesse n'est pas géré
		//  en debug la vitesse s'affiche dans la vue serial monitor
		DEBUG_PRINT(vitesse);
	}
}

void Vitesse::interrupt() {
//	millisInt0 = millisInt1;
	millisInt1 = millisInt2;
	millisInt2 = millis();
}
