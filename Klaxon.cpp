// #define DEBUG

#include <Arduino.h>
#include <AceButton.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "Klaxon.h"

using namespace ace_button;

Klaxon::Klaxon() : config(), button(KLAXON_BUTTON_PIN) {
  pinMode(KLAXON_PIN, OUTPUT);
  pinMode(KLAXON_BUTTON_PIN, INPUT_PULLUP);
  button.setButtonConfig(&config);
  config.setEventHandler(handleEvent);
}

void Klaxon::go() {
  button.check();
}

void Klaxon::handleEvent(AceButton*, uint8_t eventType, uint8_t) {
  switch (eventType) {
    case AceButton::kEventPressed:
      tone(KLAXON_PIN, 1000);
      // Pour test avec buzzer, sinon :
      // digitalWrite(KLAXON_PIN, HIGH);
      break;
    case AceButton::kEventReleased:
      noTone(KLAXON_PIN);
      // Pour test avec buzzer, sinon :
      // digitalWrite(KLAXON_PIN, LOW);
      break;
  }
}
