// #define DEBUG

#include <Arduino.h>
#include "Debug.h"
#include <AceButton.h>
#include <Chrono.h>
#include "ArduinoPins.h"
#include "Cligno.h"

using namespace ace_button;

bool Cligno::cgOn = false;
bool Cligno::ledCgOn = false;
bool Cligno::cdOn = false;
bool Cligno::ledCdOn = false;
bool Cligno::warningOn = false;
Chrono Cligno::chronoBlink;
Chrono Cligno::chronoMax;

Cligno::Cligno() : config(), buttonG(CG_BUTTON_PIN, HIGH, 0), buttonD(CD_BUTTON_PIN, HIGH, 1) {
  pinMode(CG_LED_PIN, OUTPUT);
  pinMode(CG_BUTTON_PIN, INPUT_PULLUP);
  buttonG.setButtonConfig(&config);
  pinMode(CD_LED_PIN, OUTPUT);
  pinMode(CD_BUTTON_PIN, INPUT_PULLUP);
  buttonD.setButtonConfig(&config);
  config.setEventHandler(handleEvent);
  // voir AceButton/src/ace_button/ButtonConfig.h et AceButton.h pour les événements
  config.setFeature(ButtonConfig::kFeatureDoubleClick);
  config.setFeature(ButtonConfig::kFeatureLongPress);
}

void Cligno::go() {
  buttonG.check();
  buttonD.check();
  if ((cgOn || cdOn) && chronoMax.hasPassed(C_TEMPO_MAX)) {
	// Si l'un des deux clignotants est en fonctionnement depuis
	// plus de C_TEMPO_MAX, on éteint tout.
    cgOn = false;
    ledCgOn = false;
    cdOn = false;
    ledCdOn = false;
    digitalWrite(CG_LED_PIN, LOW);
    digitalWrite(CG_LED_PIN, LOW);
  } else if (cgOn && chronoBlink.hasPassed(C_TEMPO_BLINK)) {
    // Si la tempo C_TEMPO_BLINK est dépassée, on inverse
	// l'état du clignotant (lumière éteinte/allumée)
    digitalWrite(CG_LED_PIN, ledCgOn ? LOW : HIGH);
    ledCgOn = !ledCgOn;
    chronoBlink.restart();
  } else if (cdOn && chronoBlink.hasPassed(C_TEMPO_BLINK)) {
    digitalWrite(CD_LED_PIN, ledCdOn ? LOW : HIGH);
    ledCdOn = !ledCdOn;
    chronoBlink.restart();
  }
  else if (warningOn && chronoBlink.hasPassed(C_TEMPO_BLINK)) {
	  digitalWrite(CD_LED_PIN, ledCdOn ? LOW : HIGH);
	  digitalWrite(CG_LED_PIN, ledCgOn ? LOW : HIGH);
	  ledCdOn = !ledCdOn;
	  ledCgOn = !ledCgOn;
	  chronoBlink.restart();
  }
}

void Cligno::handleEvent(AceButton* button, uint8_t eventType, uint8_t) {
  DEBUG_PRINT(eventType);
  switch (eventType) {
    case AceButton::kEventClicked:
    case AceButton::kEventLongPressed:
      if (button->getId() == 0) {
        // bouton cligno gauche cliqué
        DEBUG_PRINT("bouton cligno gauche cliqué");
        if (warningOn) {
          warningOn = false;
          digitalWrite(CD_LED_PIN, LOW);
          digitalWrite(CG_LED_PIN, LOW);
        } else {
			cgOn = !cgOn;
			// On force l'arrêt du clignotant droit et des warnings
			cdOn = false;
			digitalWrite(CD_LED_PIN, LOW);
			if (cgOn) {
			  // début clignotement gauche on allume la lampe
			  digitalWrite(CG_LED_PIN, HIGH);
			  ledCgOn = true;
			  //  on démarre la tempo pour durée du clignotement
			  chronoBlink.restart();
			  // on démarre la tempo pour arrêt automatique du clignotant
			  chronoMax.restart();
			} else {
			  // arrêt du clignotant on éteint la lampe
			  digitalWrite(CG_LED_PIN, LOW);
			}
         }
      } else { // button.getId() == 1
        // bouton cligno droit cliqué
        DEBUG_PRINT("bouton cligno droit cliqué");
        if (warningOn) {
		  warningOn = false;
		  digitalWrite(CD_LED_PIN, LOW);
		  digitalWrite(CG_LED_PIN, LOW);
		} else {
			cdOn = !cdOn;
			// On force l'arrêt du clignotant gauche et des warnings
			cgOn = false;
			digitalWrite(CG_LED_PIN, LOW);
			if (cdOn) {
			  // début clignotement droit on allume la lampe
			  digitalWrite(CD_LED_PIN, HIGH);
			  ledCdOn = true;
			  chronoBlink.restart();
			  chronoMax.restart();
			} else {
			  digitalWrite(CD_LED_PIN, LOW);
			}
		}
      }
      break;
    case AceButton::kEventDoubleClicked:
      warningOn = !warningOn;
      cgOn = false;
      cdOn = false;
      if (warningOn) {
		// début warning
		digitalWrite(CD_LED_PIN, HIGH);
		digitalWrite(CG_LED_PIN, HIGH);
	    ledCdOn = true;
	    ledCgOn = true;
		chronoBlink.restart();
	  } else {
		digitalWrite(CD_LED_PIN, LOW);
		digitalWrite(CG_LED_PIN, LOW);
	  }
      break;
  }
}
