#ifndef STOPPOINTMORT_H_
#define STOPPOINTMORT_H_

#include <AceButton.h>
#include "ArduinoPins.h"

class PointMort {
    ace_button::ButtonConfig config;
    ace_button::AceButton button;
    void static handleEvent(ace_button::AceButton*, uint8_t, uint8_t);
  public:
    PointMort();
    void go();
};

#endif
