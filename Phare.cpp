// #define DEBUG

#include <Arduino.h>
#include <AceButton.h>
#include "Debug.h"
#include "ArduinoPins.h"
#include "Phare.h"

using namespace ace_button;

// https://stackoverflow.com/questions/195207/unresolved-external-symbol-on-static-class-members
bool Phare::phareOn = false;

Phare::Phare(): config(), button(PHARE_BUTTON_PIN) {
  pinMode(PHARE_LED_PIN, OUTPUT);
  pinMode(PHARE_BUTTON_PIN, INPUT_PULLUP);
  button.setButtonConfig(&config);
  config.setEventHandler(handleEvent);
  // simple clic pour bascule phare allumé/éteint
  // voir AceButton/src/ace_button/ButtonConfig.h et AceButton.h pour les événements
  config.setFeature(ButtonConfig::kFeatureClick);
  // clic long pour appel de phare
  config.setFeature(ButtonConfig::kFeatureLongPress);
  // supprime kEventReleased après kEventClicked
  config.setFeature(ButtonConfig::kFeatureSuppressAfterClick);
  // si click < 200ms alors bascule allumé/éteint
  // si click > 210ms alors appel de phare
  // voir ButtonConfig.h
  config.setLongPressDelay(210);
}

void Phare::go() {
  button.check();
}

void Phare::handleEvent(AceButton*, uint8_t eventType, uint8_t) {
  switch (eventType) {
    case AceButton::kEventClicked:
      // bascule phare allumé/éteint
      digitalWrite(PHARE_LED_PIN, Phare::phareOn ? LOW : HIGH);
      Phare::phareOn = !Phare::phareOn;
      break;
    case AceButton::kEventReleased:
      // pour arrêt du phare après appel de phare
      //  mais pas après bascule phare allumé/éteint
      //  grâce à kFeatureSuppressAfterClick
      digitalWrite(PHARE_LED_PIN, LOW);
      break;
    case AceButton::kEventLongPressed:
      // appel de phare
      digitalWrite(PHARE_LED_PIN, HIGH);
  }
}
